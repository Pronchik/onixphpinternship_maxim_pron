<?php
namespace lesson2;

class User{
    private string $name;
    private float $balance;

    public function __construct($name, $balance){
        $this->name = $name;
        $this->balance = $balance;
    }

    public function getName(){
        return $this->name;
    }

    public function getBalance(){
        return $this->balance;
    }

    public function __toString(){
        return "У пользователя ".$this->getName()." сейчас на счету ".$this->getBalance().PHP_EOL;
    }

    public function giveMoney($user, $amount){
        if($this->balance < $amount){
            return "Error".PHP_EOL;
        }
        $user->balance += $amount;
        $this->balance -= $amount;
        return "Пользователь ".$this->getName()." перечислил $amount пользователю ".$user->getName().PHP_EOL;
    }
}

abstract class Product{
    private string $name;
    private float $price;
    private User $owner;
    static private array $products = array();

    function __construct($name, $price, $owner){
        $this->name = $name;
        $this->price = $price;
        $this->owner = $owner;
    }

    public static function registerProduct($product){
        if(in_array($product,self::$products,true)){
            return "Уже есть";
        }
        else{
            array_push(self::$products,$product);
        }
        return self::$products;
    }

    public static function  getProductIterator(){
        return new class (self::$products) implements \Iterator{
            private int $position = 0;
            private array $array;

            public function __construct($array)
            {
                $this->array = $array;
            }

            public function valid()
            {
                return isset($this->array[$this->position]);
            }
            public function next()
            {
                ++$this->position;
            }

            public function current()
            {
                return $this->array[$this->position];
            }
            public function rewind()
            {
                $this->position = 0;
            }
            public function key()
            {
                return $this->position;
            }
        };
    }

    public function __toString(): string
    {
        return "Название: " .$this->name . ', Владелец: ' . $this->owner->getName() . ', Цена:' . $this->price .
            ';'.PHP_EOL;
    }

}

class Processor extends Product{
    private float $frequency;

    public function __construct($name, $price, $owner, $frequency)
    {
        parent::__construct($name, $price, $owner);
        $this->frequency = $frequency;
    }
}

class Ram extends Product{
    private string $type;
    private float $memory;

    public function __construct($name, $price, $owner, $type, $memory)
    {
        parent::__construct($name, $price, $owner);
        $this->type = $type;
        $this->memory = $memory;
    }
}
