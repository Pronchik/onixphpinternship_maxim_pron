CREATE DATABASE market;

DROP TABLE IF EXISTS users;
CREATE TABLE IF NOT EXISTS users
(
    id   SERIAL PRIMARY KEY,
    first_name VARCHAR(15) NOT NULL,
    balance MONEY
);

DROP TABLE IF EXISTS types;
CREATE TABLE IF NOT EXISTS types
(
    id          SERIAL PRIMARY KEY,
    type        VARCHAR(30) NOT NULL
);

DROP TABLE IF EXISTS products;
CREATE TABLE IF NOT EXISTS products
(
    id          SERIAL PRIMARY KEY,
    owner_id    integer     NOT NULL,
    type_id     integer     NOT NULL,
    name  VARCHAR(30) NOT NULL,
    price MONEY NOT NULL,
    CONSTRAINT products_owner_id_fkey FOREIGN KEY (owner_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT products_type_id_fkey FOREIGN KEY (type_id) REFERENCES types (id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO users(first_name, balance) 
VALUES ('Maxim', 6000),
       ('Anton', 5000),
       ('Dima', 5500),
       ('Victor', 4000);

INSERT INTO types(type) 
VALUES ('Processor'),
       ('Ram');

INSERT INTO products(owner_id,type_id,name,price) 
VALUES (1, 1, 'Intel1', 5000),
       (1, 2, 'DDR3', 1500),
       (2, 1, 'AMD', 4000 ),
       (3, 2, 'DDR1', 2000),
       (4, 1, 'Intel3', 5000);

SELECT users.first_name,products.name
FROM products
LEFT JOIN users
ON products.owner_id = users.id
ORDER BY name;

UPDATE products SET owner_id = 3 WHERE id = 5;

DELETE FROM  users 
  WHERE id = 1;

SELECT users.first_name,COUNT(products.owner_id)
FROM users
LEFT JOIN products
ON products.owner_id = users.id
GROUP BY users.first_name,products.owner_id;

ALTER TABLE users ADD COLUMN  email VARCHAR(30) UNIQUE;

ALTER SEQUENCE users_id_seq RESTART WITH 10000;

ALTER TABLE users ADD COLUMN birth_date DATE CHECK (birth_date < current_date);

ALTER TABLE users ADD COLUMN age float;


CREATE OR REPLACE FUNCTION set_age() RETURNS TRIGGER AS
$$
BEGIN
    UPDATE users SET age = date_part('year', age(NEW.birth_date)) where users.id = NEW.id;
    RETURN NULL;
END;
$$ 
LANGUAGE plpgsql;
CREATE TRIGGER set_age
    AFTER insert
    ON users FOR EACH ROW
EXECUTE FUNCTION set_age();


BEGIN;

INSERT INTO users (first_name, balance, birth_date)
VALUES ('AAAA', '100', '2020-09-15');


INSERT INTO products (name, price,type_id, owner_id)
VALUES ('AMD1', '1500','1', '4'),
       ('AMD2', '1000','1', '2'),
       ('AMD3', '5200', '1','3');
COMMIT;
