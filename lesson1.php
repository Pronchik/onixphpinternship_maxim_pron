<?php
namespace lesson1;

class User{
    private string $name;
    private float $balance;

    public function __construct($name, $balance){
        $this->name = $name;
        $this->balance = $balance;
    }

    public function getName(){
        return $this->name;
    }

    public function getBalance(){
        return $this->balance;
    }

    public function printStatus(){
        return "У пользователя ".$this->getName()." сейчас на счету ".$this->getBalance().PHP_EOL;
    }

    public function giveMoney($user, $amount){
        if($this->balance < $amount){
            return "Error".PHP_EOL;
        }
        $user->balance += $amount;
        $this->balance -= $amount;
        return "Пользователь ".$this->getName()." перечислил $amount пользователю ".$user->getName().PHP_EOL;
    }
}
