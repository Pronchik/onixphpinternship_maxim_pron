<?php
namespace lesson3;

class User{
    private string $name;
    private float $balance;

    public function __construct($name, $balance){
        $this->name = $name;
        $this->balance = $balance;
    }

    public function getName(){
        return $this->name;
    }

    public function getBalance(){
        return $this->balance;
    }

    public function __toString(){
        return "У пользователя ".$this->getName()." сейчас на счету ".$this->getBalance().PHP_EOL;
    }

    public function giveMoney($user, $amount){
        if($this->balance < $amount){
            return "Error".PHP_EOL;
        }
        $user->balance += $amount;
        $this->balance -= $amount;
        return "Пользователь ".$this->getName()." перечислил $amount пользователю ".$user->getName().PHP_EOL;
    }

    public function listProduct(){
        $products = [];

        foreach (Product::getProductIterator() as $product){
            if ($product->getOwner()->getName() === $this->getName()){
                $products[] = $product;
            }
        }
        return $products;
    }

    public function sellProduct($user, $product){
        if($product->getOwner()->getName() === $this->getName()){
            if($user->balance < $product->getPrice()){
                return "Пользователь " . $user->getName() . " не может перечислил "
                    . $product->getPrice() . " пользователю "
                    . $this->getName() . " так как имеет только " . $user->getBalance().PHP_EOL;
            }
            $this->balance += $product->getPrice();
            $user->balance -= $product->getPrice();
            $product->setOwner($user);

            return "Пользователь " .$this->getName() ." продал продукт ". $product->getName()
                ." за ". $product->getPrice()." пользователю " .$user->getName().PHP_EOL;
        }
        return "Пользователь " .$this->getName() ." не может продать продукт "
            . $product->getName()." ".$product->getPrice()
            ." так как он пренадлежит ". $product->getOwner()->getName().PHP_EOL;
    }
}

abstract class Product{
    private string $name;
    private float $price;
    private User $owner;
    static private array $products = array();

    function __construct($name, $price, $owner){
        $this->name = $name;
        $this->price = $price;
        $this->owner = $owner;
        self::registerProduct($this);
    }

    public function getOwner()
    {
        return $this->owner;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setOwner(User $user)
    {
        $this->owner = $user;
    }

    private static function registerProduct($product){
        if(in_array($product,self::$products,true)){
            return "Уже есть";
        } else{
            array_push(self::$products,$product);
        }
        return self::$products;
    }

    public static function createRandomProduct(User $user){
        $type = rand(0,1);
        $name = rand(10,1000);
        
        if($type == 0){
            $randomProduct = new Ram("Ram ".$name,
                rand(100,2000),
                $user,
                rand(1,100),
                rand(1,100));
        }
        if($type == 1){
            $randomProduct = new Processor('Proc '.$name,
                rand(500,8000),
                $user,
                rand(1,5));
        }
        return $randomProduct;
    }

    public static function  getProductIterator(){
        return new class (self::$products) implements \Iterator{
            private int $position = 0;
            private array $array;

            public function __construct($array)
            {
                $this->array = $array;
            }

            public function valid()
            {
                return isset($this->array[$this->position]);
            }

            public function next()
            {
                ++$this->position;
            }

            public function current()
            {
                return $this->array[$this->position];
            }

            public function rewind()
            {
                $this->position = 0;
            }
            public function key()
            {
                return $this->position;
            }
        };
    }

    public function __toString(): string
    {
        return "Название: " .$this->name . ', Владелец: ' . $this->owner->getName() . ', Цена:' . $this->price .
            ';'.PHP_EOL;
    }
}

class Processor extends Product{
    private float $frequency;

    public function __construct($name, $price, $owner, $frequency)
    {
        parent::__construct($name, $price, $owner);
        $this->frequency = $frequency;
    }
}

class Ram extends Product{
    private string $type;
    private float $memory;

    public function __construct($name, $price, $owner, $type, $memory)
    {
        parent::__construct($name, $price, $owner);
        $this->type = $type;
        $this->memory = $memory;
    }
}


$user = new User("Maxim",1000);
$user1 = new User("Anton",2000);

$prod = new Processor("Intel",4500,$user,3.9);
$prod1 = new Ram("AAA",500,$user1,"DDR",250);
$prod3 = Product::createRandomProduct($user1);
$prod4 = Product::createRandomProduct($user);

echo $user1->sellProduct($user,$prod3);

$products = Product::getProductIterator();

foreach ($products as $p) {
    echo $p;
}
